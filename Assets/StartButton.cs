using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{
    [SerializeField] private StartCanvas startCanvas;
    Button button => GetComponent<Button>();
    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(StartGame) ;
    }

    public void StartGame()
    {
        MusicPlayer.Instance.SwitchMusic(1);
        startCanvas.StartGame();

    }
}
