using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiateCrab : MonoBehaviour
{
    private void Start()
    {
        if(GameManager.reloadCheckPoint == true)
        {
            transform.position = new Vector2(0, 3.57f);
            GlobalInputController.Instance.playerAction.Player.Enable();
            GlobalInputController.Instance.playerAction.UI.Enable();

        }
    }
    private void OnEnable()
    {
        StartCanvas.OnStartGame += EnterCrab;
    }

    private void OnDisable()
    {
        StartCanvas.OnStartGame -= EnterCrab;
    }

    public void EnterCrab()
    {
        transform.DOMoveY(3.57f,1f).OnComplete(()=>
        {
            GlobalInputController.Instance.playerAction.Player.Enable();
            GlobalInputController.Instance.playerAction.UI.Enable();
            Spawner.allowSpawn = true;
        });
    }
}
