using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MusicPlayer : Singleton<MusicPlayer>
{
    [SerializeField] private List<AudioSource> audioSources= new List<AudioSource>();
    [SerializeField] private List<float> originalValue = new List<float>();
    private AudioSource currentMusic;
    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (Instance != this)
        {
            Destroy(gameObject);
        }

        for (int i = 0; i < audioSources.Count; i++)
        {
            originalValue.Add(audioSources[i].volume);
            audioSources[i].volume = 0;
        }
    }

    public void SwitchMusic(int musicIndex)
    {
        if (musicIndex == -1)
        {
            currentMusic.DOFade(0, 1f);
            return;
        }

        if (currentMusic == audioSources[musicIndex]) return;


       if(currentMusic)
        {
            currentMusic.DOFade(0, 1f).OnComplete(() =>
            {
                currentMusic = audioSources[musicIndex];
                currentMusic.DOFade(originalValue[musicIndex], 1f);
                currentMusic.Play();

            });
        }
        else
        {
            currentMusic = audioSources[musicIndex];
            currentMusic.DOFade(originalValue[musicIndex], 1f);
            currentMusic.Play();
        }
    }
}
