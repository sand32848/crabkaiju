using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiateUI : MonoBehaviour
{
    [SerializeField] private RectTransform playerUI;
    [SerializeField] private RectTransform waveCounter;

    private void Start()
    {
        if (GameManager.reloadCheckPoint)
        {
            MoveUI();
        }
    }

    private void OnEnable()
    {
        StartCanvas.OnStartGame += MoveUI;
    }

    private void OnDisable()
    {
        StartCanvas.OnStartGame -= MoveUI;

    }

    public void MoveUI()
    {
        playerUI.DOAnchorPosX(0f, 1f);
        waveCounter.DOAnchorPosX(557f, 1f);

    }
}
