using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitalEnemy : MonoBehaviour
{
    MoveModule moveModule => GetComponent<MoveModule>();
    [SerializeField] private Collider2D col2D;

    private void Start()
    {
        int scaleX = UnityEngine.Random.Range(0, 2) == 1 ? 1 : -1;

        transform.localScale = new Vector2(scaleX, 1);
    }

    public void EnableCollision()
    {
        col2D.enabled = true;
    }
    private void OnEnable()
    {
        moveModule.OnMoveComplete += EnableCollision;
    }

    private void OnDisable()
    {
        moveModule.OnMoveComplete -= EnableCollision;

    }
}
