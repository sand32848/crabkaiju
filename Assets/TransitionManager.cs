using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : Singleton<TransitionManager>
{
    [SerializeField] private CanvasGroup canvasGroup;
    private void Awake()
    {
        DontDestroyOnLoad(this);

        if(Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void CallTransion()
    {
        canvasGroup.DOFade(1, 0.5f).OnComplete(() => { canvasGroup.DOFade(0, 0.5f); SceneManager.LoadScene(SceneManager.GetActiveScene().name) ;
              } );
    }
}
