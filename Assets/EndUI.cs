using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndUI : MonoBehaviour
{
    [SerializeField] private CanvasGroup transition;
    [SerializeField] GameObject endImage;
    [SerializeField] private RectTransform gameoverText;
    [SerializeField] private RectTransform mainMenuButton;

    private void OnEnable()
    {
        BossDeath.onCallCutscene += startEndCutscene;
    }

    public void startEndCutscene()
    {
        var sequence = DOTween.Sequence();

        sequence.Append(transition.DOFade(0, 2f).OnComplete(()=> { GlobalInputController.Instance.playerAction.Player.Disable(); MusicPlayer.Instance.SwitchMusic(0); }));
        sequence.Append(transition.DOFade(1, 2f).OnComplete(()=> endImage.SetActive(true)));;
        sequence.Append(transition.DOFade(0, 2f));
        sequence.Append(gameoverText.DOAnchorPosY(65,1f));
        sequence.Append(mainMenuButton.DOAnchorPosY(-152, 1f));
    }
}
