using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    private Health health => GetComponent<Health>();
    private GrabModule grabModule => GetComponent<GrabModule>();
    [SerializeField] private List<GameObject> gameObjectParticle = new List<GameObject>();
    [SerializeField] private ParticleSystem particle;

    void Start()
    {
        grabModule.OnEat += SpawnParticle;
        health.OnHealthOut += SpawnParticle;
    }


    private void OnDisable()
    {
        grabModule.OnEat -= SpawnParticle;
        health.OnHealthOut -= SpawnParticle;
    }

    public void SpawnParticle()
    {
        if (gameObjectParticle.Count > 0) 
        {
            Destroy(Instantiate(gameObjectParticle[Random.Range(0, gameObjectParticle.Count)], transform.position, Quaternion.identity), 1.5f);
        } 

        if(particle) Instantiate(particle, transform);

    }
}
