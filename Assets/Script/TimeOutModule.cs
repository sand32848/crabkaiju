using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeOutModule : MonoBehaviour
{
    [SerializeField] private float timer;
    private float warningTime = 2f;

    void Update()
    {
        timer -= Time.deltaTime;

        if(timer<= 0)
        {
            Destroy(gameObject);
        }

        if(timer <= warningTime)
        {

        }
    }
}
