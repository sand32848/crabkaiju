using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBullet : Bullet
{
    public override void OnHitTrigger(Collider2D _collision)
    {
        if(_collision.TryGetComponent(out Health health))
        {
            health.ReduceHealth(damage);
        }
    }
}
