using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class Health : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    [SerializeField] private bool destroyOnDeath;
    [field: SerializeField] public bool invul { get; private set; }

    private int curHealth;
    public Action OnHealthOut;
    public Action<int, int> OnHealthChange;
    public Action OnReduceHealth;
    private void Start()
    {
        GlobalInputController.Instance.playerAction.Player.Debug.started += Debug;
        curHealth = maxHealth;
    }

    private void OnEnable()
    {
        BossHealth.OnBossDeath += BossCallBack;
        BossSpawnState.OnBossSpawn += KillAllEnemyCallBack;
    }

    public void ReduceHealth(int damage)
    {
        if (invul) return;

        curHealth -= damage;

        if(curHealth <= 0)
        {
            HealthOutCallback();
            if (destroyOnDeath) Destroy(gameObject);
        }

        curHealth = Mathf.Clamp(curHealth, 0, maxHealth);

        OnReduceHealth?.Invoke();
        OnHealthChange?.Invoke(curHealth, maxHealth);
    }

    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.Player.Debug.started -= Debug;
        BossHealth.OnBossDeath -= BossCallBack;
        BossSpawnState.OnBossSpawn -= KillAllEnemyCallBack;

    }

    public void IncreaseHealth(int health)
    {
        curHealth += health;

        curHealth = Mathf.Clamp(curHealth, 0, maxHealth);
        OnHealthChange?.Invoke(curHealth, maxHealth);
    }

    public void HealthOutCallback()
    {
         OnHealthOut?.Invoke();
    }

    public void Debug(InputAction.CallbackContext ctx)
    {
        ReduceHealth(999);
    }

    public void BossCallBack()
    {
        if(gameObject.tag == "Enemy" && !GetComponent<BossHealth>())
        {
            ReduceHealth(9999);
        }
        else
        {
            invul = true;
        }
    }

    public void KillAllEnemyCallBack()
    {
        if (gameObject.tag == "Enemy" && !GetComponent<BossHealth>())
        {
            Destroy(gameObject);

            GetComponent<ParticleSpawner>().SpawnParticle();
        }
    }

    public void setInvul(bool _invul)
    {
        invul = _invul;
    }
}
