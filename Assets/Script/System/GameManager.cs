using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int targetCount;
    [SerializeField] private List<WaveScriptable> waveScriptables = new List<WaveScriptable>();
    private int currentEnemy;

    public static Action<WaveScriptable,int> OnWaveClear;
    public static Action<int,int> OnUpdateEnemyCount;
    public static Action OnBossWaveReach;

    public static int currentWaveIndex;
    public static bool reloadCheckPoint;
    private bool inBossState;

    private void Start()
    {
        print(currentWaveIndex);
        OnWaveClear?.Invoke(waveScriptables[currentWaveIndex], currentWaveIndex);
        targetCount = waveScriptables[currentWaveIndex].enemyCount;
        OnUpdateEnemyCount?.Invoke(currentEnemy,targetCount);
    }

    private void OnEnable()
    {
        EnemyDeathModule.OnEnemyDeath += increaseKillCount;
    }

    private void OnDisable()
    {
        EnemyDeathModule.OnEnemyDeath -= increaseKillCount;
    }

    public void increaseKillCount()
    {
        if (inBossState) return;

        currentEnemy++;

        if(targetCount == currentEnemy)
        {
            currentEnemy = 0;
            currentWaveIndex += 1;

            if(currentWaveIndex == 6)
            {
                inBossState = true;
                OnBossWaveReach?.Invoke();
            }

            if (currentWaveIndex >= waveScriptables.Count) return;

            OnWaveClear?.Invoke(waveScriptables[currentWaveIndex],currentWaveIndex);
            targetCount = waveScriptables[currentWaveIndex].enemyCount;
        }

        OnUpdateEnemyCount?.Invoke(currentEnemy, targetCount);
    }
}
