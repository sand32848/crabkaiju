using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatMortarExplosion : Bullet
{
    public override void OnHitTrigger(Collider2D _collision)
    {
        if(_collision.TryGetComponent(out Health health))
        {
            health.ReduceHealth(damage);
        }
    }

    public override void OnSpawn(Vector2 bulletDirection)
    {
        transform.DOScale(20, speed).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            transform.DOScale(0, 0.5f).OnComplete(() =>
            {
                Destroy(gameObject);
            });
        });

    }
}
