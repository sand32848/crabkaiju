using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbital : Bullet
{
    [SerializeField] private float duration;
    [SerializeField] private float delayTime;
    [SerializeField] private float trackTime;
    [SerializeField] private float damageTick;
    [SerializeField] private float radius;
    [SerializeField] private LayerMask hitMask;
    [SerializeField] private bool isPlayer;
    [SerializeField] private GameObject laserSprite;
    [SerializeField] private GameObject aimSprite;
    private float _damageTick;
    private bool firstTimeDamage;

    private void Start()
    {
        _damageTick = damageTick;
    }

    private void Update()
    {
        if (trackTime > 0 && !isPlayer)
        {
            transform.position = player.transform.position;

            trackTime -= Time.deltaTime;

            return;
        }

        delayTime -= Time.deltaTime;

        if (delayTime >= 0) return;

        if (!firstTimeDamage)
        {
            DoDamage();
            firstTimeDamage = true;
        }

        aimSprite.SetActive(false); 
        laserSprite.SetActive(true);

        CameraShakeController.Instance.ShakeCamera(1, 1f);

        duration -= Time.deltaTime;
        _damageTick -= Time.deltaTime;

        if (duration <= 0) Destroy(gameObject);

        if(_damageTick <= 0)
        {
            DoDamage();
            _damageTick = damageTick;
        }
    }

    public override void OnSpawn(Vector2 dirToMouse)
    {
        Vector2 camPos = Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>());
        if (isPlayer)
        {
            transform.position = new Vector3(camPos.x,camPos.y,0) ;
        }
        else
        {
            transform.position = player.transform.position;
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    public void DoDamage()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, radius, hitMask);

        for (int i = 0; i < collider.Length; i++)
        {
            if (collider[i].TryGetComponent(out Health health))
            {
                health.ReduceHealth(damage);
            }
        }

        _damageTick = damageTick;
    }
}
