using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] protected int damage;
    [SerializeField] protected float speed;
    [SerializeField] protected GameObject hitParticle;
    protected GameObject player;
    protected Rigidbody2D rb;
    protected Animator animator;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnHitTrigger(collision);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        OnStay(collision);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnHitCollision(collision);
    }

    public virtual void OnSpawn(Vector2 bulletDirection)
    {
        
    }

    public virtual void OnHitTrigger(Collider2D _collision)
    {

    }

    public virtual void OnHitCollision(Collision2D _collision)
    {

    }

    public virtual void OnStay(Collider2D _collision)
    {

    }

    public void getPlayer(GameObject _player)
    {
        player = _player;
    }

    public void InstanceHitParticle()
    {
        if(hitParticle) Destroy(Instantiate(hitParticle, transform.position, Quaternion.identity),0.3f);
    }
}
