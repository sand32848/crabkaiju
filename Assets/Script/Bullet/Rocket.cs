using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : Bullet
{
    [SerializeField] private float explosionRadius;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float lifeTime;
    [SerializeField] private float trackTime;
    [SerializeField] private bool isPlayer;
    [SerializeField] private LayerMask hitMask;
    [SerializeField] private GameObject explosionEffect;
    [SerializeField] private GameObject crossHair;
    private GameObject target;

    private void Start()
    {
        GameObject[] enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
        target = enemyArray[UnityEngine.Random.Range(0, (enemyArray.Length - 1))];
    }

    private void Update()
    {
        trackTime -= Time.deltaTime;

        if (!isPlayer)
        {
            Vector2 rocketDir = player.transform.position - transform.position;

            var angle = Mathf.Atan2(rocketDir.y, rocketDir.x) * Mathf.Rad2Deg;
            var angleCal = Quaternion.AngleAxis(angle, Vector3.forward);

            //Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.left);

            var step = rotateSpeed * Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, angleCal, step);

            rb.velocity = transform.right * speed;

        }
        else
        {
            if(target == null)
            {
                GameObject[] enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
                target = enemyArray[UnityEngine.Random.Range(0, (enemyArray.Length - 1))];
            }

            lifeTime -= Time.deltaTime;

            Vector2 rocketDir = target.transform.position - transform.position;
            var angle = Mathf.Atan2(rocketDir.y, rocketDir.x) * Mathf.Rad2Deg;
            var angleCal = Quaternion.AngleAxis(angle, Vector3.forward);
            var step = rotateSpeed * Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, angleCal, step);

            crossHair.SetActive(target ? true : false);
            if (crossHair) crossHair.transform.localPosition = transform.InverseTransformPoint(target.transform.position);

            rb.velocity = transform.right * speed;

            if (Vector2.Distance(transform.position,target.transform.position) <= 1f || lifeTime <= 0)
            {
                Destroy(Instantiate(explosionEffect, transform.position, Quaternion.identity), 1);
                Destroy(gameObject);
                Explode();
            }
        }
    }

    public override void OnSpawn(Vector2 attackDir)
    {
        if (isPlayer)
        {
            rb.AddForce(attackDir * speed, ForceMode2D.Impulse);
        }
    }


    public override void OnHitTrigger(Collider2D _collision)
    {
        Destroy(Instantiate(explosionEffect,transform.position,Quaternion.identity),1);
        Destroy(gameObject);
        Explode();
    }

    public void Explode()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position,explosionRadius,hitMask);

        for(int i = 0; i < collider.Length; i++)
        {
            if(collider[i].TryGetComponent(out Health health))
            {
                health.ReduceHealth(damage);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position,explosionRadius);
    }
}
