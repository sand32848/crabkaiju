using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBullet : Bullet
{
    public override void OnSpawn(Vector2 dirToMouse)
    {
        rb.AddForce(dirToMouse * speed, ForceMode2D.Impulse);
    }

    public override void OnHitTrigger(Collider2D _collision)
    {
        if (_collision.transform.TryGetComponent(out Health health))
        {
            health.ReduceHealth(damage);
        }

        InstanceHitParticle();

        Destroy(gameObject);
    }

}
