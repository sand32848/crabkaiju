using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Mortar : Bullet
{
    [SerializeField] private GameObject Explode;
    [SerializeField] private float scale;
    [SerializeField] private float explosionRadius;
    [SerializeField] private LayerMask hitMask;
    [SerializeField] private int particleDamage;
    [SerializeField] private bool isPlayer;


    public override void OnSpawn(Vector2 dirToMouse)
    {
        Vector2 camPos = Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>());

        Vector2 targetPos = isPlayer ? camPos : dirToMouse;

        transform.DOScale(scale, speed / 2).SetEase(Ease.OutCubic).OnComplete(() => { transform.DOScale(1, speed / 2).SetEase(Ease.InQuint); }) ;
        transform.DOMove(targetPos,speed).SetEase(Ease.OutCubic).OnComplete(()=> { DoDamage(); CameraShakeController.Instance.ShakeCamera(3f, 2f);Destroy(gameObject); InstanceParticle(); });
    }

    public void DoDamage()
    {
        if (!isPlayer) return;

        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, explosionRadius, hitMask);

        for (int i = 0; i < collider.Length; i++)
        {
            if (collider[i].TryGetComponent(out Health health))
            {
                health.ReduceHealth(damage);
            }
        }
    }

    public void InstanceParticle()
    {
        GameObject _explosion = Instantiate(Explode, transform.position, Quaternion.identity);

        ParticleCollision particleCollision = _explosion.GetComponent<ParticleCollision>();

        _explosion.GetComponent<Bullet>()?.OnSpawn(Vector2.zero);

       particleCollision.OnParticleCollide += ParticleDamage;
    }

    public void ParticleDamage(GameObject target)
    {
        if (target.tag != "Enemy") return;
        target.GetComponent<Health>().ReduceHealth(particleDamage);
    }



    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
