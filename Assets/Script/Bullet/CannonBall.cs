using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : Bullet
{
    private Vector3 lastVelocity;
    [SerializeField] private float deviation;
    [SerializeField] private AnimationCurve speedCurve;
    [SerializeField] private int bounceAmount;
    [SerializeField] private float minimumMagnitude;
    private int _bounceAmount;
    private float time;
    private ParticleSystem particle;
    private float initialForce;

    private void Start()
    {
        particle = GetComponent<ParticleSystem>();
    }

    public override void OnSpawn(Vector2 dirToMouse)
    {
        rb.AddForce(dirToMouse * speed, ForceMode2D.Impulse);
        initialForce = rb.velocity.magnitude;
    }

    private void Update()
    {
        lastVelocity = rb.velocity;

        if(_bounceAmount >= bounceAmount)
        {
            time += Time.deltaTime;
        }
        else
        {
            rb.velocity = rb.velocity.normalized * initialForce;
        }

        if(_bounceAmount >= bounceAmount && minimumMagnitude >= rb.velocity.magnitude)
        {
            Destroy(gameObject);
        }

    }

    public override void OnHitCollision(Collision2D _collision)
    {
        speed = lastVelocity.magnitude;

        if (_collision.transform.TryGetComponent(out Health health))
        {
            health.ReduceHealth(damage);
        }

        if (_collision.transform.tag == "Wall")
        {
            CameraShakeController.Instance.ShakeCamera(1f, 0.5f);

            _bounceAmount += 1;

            Vector2 reflectDir = Vector3.Reflect(lastVelocity.normalized, _collision.contacts[0].normal);

            float randomSpread = UnityEngine.Random.Range(-deviation, deviation);

            reflectDir = Quaternion.AngleAxis(randomSpread, Vector3.forward) * reflectDir;

            rb.velocity = reflectDir * Mathf.Max(speed, 0) * speedCurve.Evaluate(time);
        }

    }
}
