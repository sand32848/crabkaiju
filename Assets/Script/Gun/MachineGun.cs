using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : Gun
{
    [SerializeField] protected float rampUpTime;
    [SerializeField] protected float fireRate;
    [SerializeField] private float spread;
    protected bool isRampUp;
    protected float _rampUpTime;
    protected float _fireRate;

    private void Start()
    {
        currentBulletAmount = bulletAmount;
        _rampUpTime = rampUpTime;
    }

    private void Update()
    {
        if (!isRampUp)
        {
            _rampUpTime -= Time.deltaTime;

            _rampUpTime = Mathf.Clamp(_rampUpTime,0, rampUpTime);
        }
    }

    public override void SingleFire()
    {
      
    }

    public override void HoldFire()
    {
        isRampUp = true;

        _rampUpTime += Time.deltaTime;

        if (_rampUpTime < rampUpTime) return;

        _fireRate  -= Time.deltaTime;

        if(_fireRate <= 0)
        {
           gunSound.Play();

            InstantBullet(spread);

            _fireRate = fireRate;
        }
     
    }

    public override void ReleaseFire()
    {
        isRampUp = false;
    }
}
