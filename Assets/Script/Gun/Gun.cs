using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] protected Bullet bulllet;
    [SerializeField] protected float gunLifeTimer;
    [SerializeField] protected int bulletAmount;
    [field : SerializeField] public int tier { get; private set; }
    [HideInInspector] public GunController gunController;
    [SerializeField] protected AudioSource gunSound;
    public int currentBulletAmount 
    { 
        get 
        { 
            return _currentBullletAmount;
        } 
        
        protected set 
        { 
            _currentBullletAmount = value;
            OnBulletChange?.Invoke(_currentBullletAmount, bulletAmount);
        } 
    }

    private int _currentBullletAmount;
    [SerializeField] private bool rotateBullet = true;
    private GameObject player => GameObject.FindGameObjectWithTag("Player");

    public static Action<int, int> OnBulletChange;

    public virtual void SingleFire()
    {
       gunSound?.Play();
       InstantBullet();
    }

    public virtual void HoldFire()
    {

    }

    public virtual void ReleaseFire()
    {

    }

    public virtual void OnSwitchGun()
    {
      
    }

    public void RestartBullet()
    {
        currentBulletAmount = bulletAmount;
        OnBulletChange?.Invoke(_currentBullletAmount, bulletAmount);
    }

    private void Update()
    {
        CheckGunRunOut();
    }

    private void Start()
    {
        currentBulletAmount = bulletAmount;
    }

    public void CheckGunRunOut()
    {
        if(currentBulletAmount <= 0)
        {
            gunController.SwitchGun(0);
        }

        if (gunLifeTimer == -1) return;

        gunLifeTimer -= Time.deltaTime;

        if (gunLifeTimer <= 0)
        {
            gunController.SwitchGun(0);
        }
    }

    public Vector2 directionToMouse()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>());

        Vector2 mouseDir = (mousePos - (Vector2)transform.position).normalized;

        return mouseDir;
    }

    public void InstantBullet(float spread = 0)
    {
        if (!bulllet) return;

        Bullet _bullet = Instantiate(bulllet, transform.position, Quaternion.identity);
        _bullet.getPlayer(player);

        Vector3 bulletDir = directionToMouse();

        if (rotateBullet)
        {
            float randomSpread = UnityEngine.Random.Range(-spread, spread);

            bulletDir = Quaternion.AngleAxis(randomSpread,Vector3.forward) * bulletDir;

            var angle = Mathf.Atan2(-bulletDir.y, -bulletDir.x) * Mathf.Rad2Deg;
            _bullet.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        _bullet.OnSpawn(bulletDir);

        currentBulletAmount -= 1;
    }
}
