using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketGun : Gun
{
    [SerializeField] private int Amount;

    public override void SingleFire()
    {
       for(int i =0; i< Amount; i++)
        {
            InstantBullet(30);
        }
    }
}
