using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class PauseUI : MonoBehaviour
{
    [SerializeField] GameObject pauseUI;
    private bool pausing;

    private void Start()
    {
        GlobalInputController.Instance.playerAction.UI.Pause.started += PauseCallBack;
    }

    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.UI.Pause.started -= PauseCallBack;

    }

    public void PauseCallBack(InputAction.CallbackContext ctx)
    {
        Pause();
    }

    public  void Pause()
    {
        pausing = !pausing;

        Time.timeScale = pausing ? 0 : 1;
        pauseUI.SetActive(pausing);

        if (pausing)
        {
            GlobalInputController.Instance.playerAction.Player.Disable();
        }
        else
        {
            GlobalInputController.Instance.playerAction.Player.Enable();
        }

    }
}
