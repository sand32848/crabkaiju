using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private void Start()
    {
        GlobalInputController.Instance.playerAction.Player.ReloadScene.started += debug;
    }

    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.Player.ReloadScene.started -= debug;

    }

    public void debug(InputAction.CallbackContext ctx)
    {
        ReloadSceneReset();
    }


    public  void ReloadSceneReset()
    {
        Spawner.allowSpawn = false;
        GameManager.currentWaveIndex = 0;
        GameManager.reloadCheckPoint = false;
        Time.timeScale = 1;
        TransitionManager.Instance.CallTransion();

    }

    public void ReloadSceneCheckPoint()
    {
        GameManager.reloadCheckPoint = true;
        Time.timeScale = 1;
        TransitionManager.Instance.CallTransion();


    }

    public void ReloadSceneMainMenu()
    {
        Spawner.allowSpawn = false;
        GameManager.reloadCheckPoint = false;
        Time.timeScale = 1;
        TransitionManager.Instance.CallTransion();


    }

    public  void ReloadSceneRestartStage()
    {
        Spawner.allowSpawn = false;
        GameManager.currentWaveIndex = 0;
        GameManager.reloadCheckPoint = true;
        Time.timeScale = 1;
        TransitionManager.Instance.CallTransion();

    }
}
