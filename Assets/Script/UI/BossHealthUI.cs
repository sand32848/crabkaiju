using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthUI : MonoBehaviour
{
    [SerializeField] private RectTransform bossUI;
    [SerializeField] private Slider bossSlider;

    private void Start()
    {
   
    }
    private void OnEnable()
    {
        BossHealth.OnUpdateBossHealth += UpdateHealthUI;
        BossSpawnState.OnBossHealthBar += InititateBossHealth;
    }

    private void OnDisable()
    {
        BossHealth.OnUpdateBossHealth -= UpdateHealthUI;
        BossSpawnState.OnBossHealthBar -= InititateBossHealth;
    }

    public void InititateBossHealth()
    {
        bossUI.DOAnchorPosX(-5, 1f).OnComplete(() =>
        {
            bossSlider.DOValue(1, 1f);
        });
    }

    public void UpdateHealthUI(int curHealth,int maxHealth)
    {
        float percentage = (float)curHealth / (float)maxHealth;
        bossSlider.value = percentage;

        if (DOTween.IsTweening("BossUIShake")) return;

            bossSlider.transform.DOShakePosition(0.5f, 5, 10, 90, true, false).SetId("BossUIShake");

    }
}
