using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveUI : MonoBehaviour
{
    [SerializeField] private RectTransform numberText;
    [SerializeField] private TextMeshProUGUI text;

    private void OnEnable()
    {
        GameManager.OnWaveClear += UpdateWaveNumber;
    }

    private void OnDisable()
    {
        GameManager.OnWaveClear += UpdateWaveNumber;
    }

    public void UpdateWaveNumber(WaveScriptable _,int waveIndex)
    {
        numberText.DOAnchorPosY(-30, 1f).OnComplete(() => 
        {
            text.text = (waveIndex + 1).ToString();
            numberText.anchoredPosition = new Vector2(numberText.anchoredPosition.x, 30);
            numberText.DOAnchorPosY(0, 1f);
        });
    }
}
