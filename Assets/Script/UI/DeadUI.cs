using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadUI : MonoBehaviour
{
    [SerializeField] private GameObject deadUI;

    private void Start()
    {
        PlayerHealth.OnPlayerDie += EnableDeadUI;
    }

    private void OnDisable()
    {
        PlayerHealth.OnPlayerDie -= EnableDeadUI;
    }

    public void EnableDeadUI()
    {
        deadUI.SetActive(true);
    }
}
