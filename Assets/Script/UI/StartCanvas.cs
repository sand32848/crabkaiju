using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCanvas : MonoBehaviour
{
    [SerializeField] RectTransform title;
    [SerializeField] RectTransform buttonHolder;
    public static Action OnStartGame;
    // Start is called before the first frame update
    void Start()
    {
        if(GameManager.reloadCheckPoint == true)
        {
            gameObject.SetActive(false);
            MusicPlayer.Instance.SwitchMusic(1);
        }
        else
        {
            MusicPlayer.Instance.SwitchMusic(0);

        }

        title.DOAnchorPosY(34,1f).SetEase(Ease.Linear);
        buttonHolder.DOAnchorPosY(-181,1f).SetEase(Ease.Linear);

        GlobalInputController.Instance.playerAction.Player.Disable();
        GlobalInputController.Instance.playerAction.UI.Disable();

    }

    public void StartGame()
    {
        title.DOAnchorPosY(500, 1f).SetEase(Ease.Linear);
        buttonHolder.DOAnchorPosY(-430, 1f).SetEase(Ease.Linear).OnComplete(() => OnStartGame?.Invoke());

    }
}
