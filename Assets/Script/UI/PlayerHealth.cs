using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    private Health health => GetComponent<Health>();
    public static Action<int,int> OnPlayerHealthDamage;
    public static Action OnPlayerDie;

    [SerializeField] private SpriteRenderer playerSprite;
    [SerializeField] private float invulTime;

    [SerializeField] private GameObject deadEffect;

    public void UpdateHealth(int _curHealth,int _maxHealth)
    {
        OnPlayerHealthDamage?.Invoke(_curHealth,_maxHealth);

        if(_curHealth <= 0)
        {
            OnPlayerDie?.Invoke();
            Instantiate(deadEffect, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        health.OnHealthChange += UpdateHealth;
        health.OnReduceHealth += ToggleInvul;

    }

    private void OnDisable()
    {
        health.OnHealthChange -= UpdateHealth;
        health.OnReduceHealth -= ToggleInvul;
    }


    public void ToggleInvul()
    {
        StartCoroutine(invulEnumerator());
    }

    IEnumerator invulEnumerator()
    {
        var sequence = DOTween.Sequence().SetLoops(-1).SetId("PlayerFlash");

        sequence.Append(playerSprite.DOFade(0, 0.05f));
        sequence.Append(playerSprite.DOFade(1, 0.05f));

        health.setInvul(true);

        yield return new WaitForSeconds(invulTime);

        DOTween.Kill("PlayerFlash");
        playerSprite.DOFade(1, 0f);
        health.setInvul(false);
 
    }
}
