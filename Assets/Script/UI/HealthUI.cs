using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    [SerializeField] private Slider healthSlider;

    private void OnEnable()
    {
        PlayerHealth.OnPlayerHealthDamage += UpdateUI;
    }

    private void OnDisable()
    {
        PlayerHealth.OnPlayerHealthDamage += UpdateUI;
    }

    public void UpdateUI(int curHealth, int maxHealth)
    {
        float percentage = (float)curHealth / (float)maxHealth;

        healthSlider.value = percentage;
    }
}
