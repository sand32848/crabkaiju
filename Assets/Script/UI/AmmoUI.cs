using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUI : MonoBehaviour
{
    [SerializeField] private Slider ammoSlider;
    [SerializeField] private List<GameObject> iconList = new List<GameObject>();

    private void OnEnable()
    {
        Gun.OnBulletChange += UpdateUI;
        GunController.OnGunSwitch += UpdateIcon;
    }

    private void OnDisable()
    {
        Gun.OnBulletChange -= UpdateUI;
        GunController.OnGunSwitch -= UpdateIcon;
    }

    public void UpdateUI(int curBullet, int maxBullet)
    {
        if(curBullet == 0 && maxBullet == 0)
        {
            ammoSlider.value = 0;
            return;
        }

        float percentage = (float)curBullet / (float)maxBullet;

        ammoSlider.value = percentage;
    }

    public void UpdateIcon(int iconIndex)
    {
        iconList.ForEach(icon => icon.gameObject.SetActive(false));
        iconList[iconIndex].SetActive(true);
 
    }
}
