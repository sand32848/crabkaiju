using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class KillCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI killText;

    private void OnEnable()
    {
        GameManager.OnUpdateEnemyCount += UpdateKillUI;
    }

    private void OnDisable()
    {
        GameManager.OnUpdateEnemyCount -= UpdateKillUI;
    }

    public void UpdateKillUI(int curEnemy,int target)
    {
        killText.text = $"{curEnemy}/{target}";
    }
}
