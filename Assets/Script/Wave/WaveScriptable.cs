using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "ScriptableObjects/Wave", order = 1)]
public class WaveScriptable : ScriptableObject
{
    public int enemyCount;
    public int currentWaveIndex;
    public List<Wave> WaveList = new List<Wave>();
}

[System.Serializable]
public class Wave
{
    public int PercentageTrigger;
    public int boostSpawnTherehold = -1;
    public int enemyInWave;
    public List<SpawnStat> WaveStat= new List<SpawnStat>();
}

[System.Serializable]
public class SpawnStat
{
    public SpawnModule SpawnModule;
    public float MinSpawnTime,MaxSpawnTime;
    public float spawnTime;
    public int maxSpawn;
    [HideInInspector] public int curSpawn;

    [HideInInspector] public float modifiedMaxSpawn;
    [HideInInspector] public float modifiedSpawnTime;

    public void reduceCurSpawn()
    {
        curSpawn -= 1;
    }

    public void increaseCurSpawn()
    {
        curSpawn += 1;
    }

    public void RestStat()
    {
        curSpawn = 0;
    }

    public void RandomSpawnTime()
    {
       spawnTime =  UnityEngine.Random.Range(MinSpawnTime, MaxSpawnTime);
    }
}