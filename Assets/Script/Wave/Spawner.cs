using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static bool allowSpawn;
    [SerializeField] private GameObject spawnPointUpLeft, SpawnPointDownRight, SpawnBottomLeft, SpawnBottomRight, SpawnInscreenUp, SpawnInscreenDown;
    [field:SerializeField] public WaveScriptable curStage { get;private set;}

    private void OnEnable()
    {
        GameManager.OnUpdateEnemyCount += checkWave;
        GameManager.OnWaveClear += LoadNewWave;
    }

    private void OnDisable()
    {
        GameManager.OnUpdateEnemyCount -= checkWave;
        GameManager.OnWaveClear += LoadNewWave;
    }

    private void Start()
    {
        initializeWave();
    }

    private void Update()
    {
        if (!allowSpawn) return;
        checkSpawn();
    }
    private void checkWave(int _curEnemy, int _maxEnemy)
    {
        if (curStage.currentWaveIndex + 1 >= curStage.WaveList.Count) return;

        if (((float)_curEnemy / (float)_maxEnemy) * 100 >= curStage.WaveList[curStage.currentWaveIndex + 1].PercentageTrigger)
        {
            curStage.currentWaveIndex++;
        }
    }

    private void checkSpawn()
    {
        for (int i = 0; i < curStage.WaveList[curStage.currentWaveIndex].WaveStat.Count; i++)
        {
            SpawnStat _spawnStat = curStage.WaveList[curStage.currentWaveIndex].WaveStat[i];

            _spawnStat.spawnTime -= Time.deltaTime;

            if (_spawnStat.curSpawn >= _spawnStat.maxSpawn) continue;

            if (_spawnStat.spawnTime >= 0) continue;

            GameObject _spawned = Spawn(_spawnStat);
            Health health = _spawned.transform.GetComponent<Health>();
            GrabModule grabModule = _spawned.transform.GetComponent<GrabModule>();
            EnemyDeathModule enemyDeathModule = _spawned.transform.GetComponent<EnemyDeathModule>();

            health.OnHealthOut += _spawnStat.reduceCurSpawn;
            grabModule.OnEat += _spawnStat.reduceCurSpawn;
            enemyDeathModule.OnDeathWall += _spawnStat.reduceCurSpawn;
            _spawnStat.increaseCurSpawn();
        }
    }

    public GameObject Spawn(SpawnStat _spawnStat)
    {
        Vector2 _spawnPos = new Vector2(0, 0);
        Vector2 _destination = new Vector2(0, 0);

        if (_spawnStat.SpawnModule.spawnMethod == SpawnMethod.Random)
        {
           int _random = UnityEngine.Random.Range(0, 2) ;

            if(_random == 0)
            {
                _spawnPos = leftRightSpawnMethod();
            }
            else
            {
                _spawnPos = bottomSpawnMethod();
                _destination = setDestination();
            }
        }

        if (_spawnStat.SpawnModule.spawnMethod == SpawnMethod.LeftRight)
        {
            _spawnPos = leftRightSpawnMethod();
        }
        else if (_spawnStat.SpawnModule.spawnMethod == SpawnMethod.Bottom)
        {
            _spawnPos = bottomSpawnMethod();
            _destination = setDestination();
        }
        else if(_spawnStat.SpawnModule.spawnMethod == SpawnMethod.InScreen)
        {
            float height = UnityEngine.Random.Range(SpawnInscreenDown.transform.position.y, SpawnInscreenUp.transform.position.y);
            float side = UnityEngine.Random.Range(SpawnInscreenDown.transform.position.x, SpawnInscreenUp.transform.position.x);
            _spawnPos = new Vector2(side, height);
        }
        else if(_spawnStat.SpawnModule.spawnMethod == SpawnMethod.Top)
        {
            float side = UnityEngine.Random.Range(SpawnInscreenDown.transform.position.x, SpawnInscreenUp.transform.position.x);
            float height = UnityEngine.Random.Range(SpawnInscreenDown.transform.position.y, SpawnInscreenUp.transform.position.y);
            _spawnPos = new Vector2(side, 7.45f);
            _destination = new Vector2(side, height);
        }

        SpawnModule _spawnModule = Instantiate(_spawnStat.SpawnModule, _spawnPos, Quaternion.identity);

        _spawnModule.OnSpawn?.Invoke(_spawnPos, _destination);

        _spawnStat.RandomSpawnTime();

        return _spawnModule.gameObject;
    }

    private Vector2 setDestination()
    {
        float destinationY = UnityEngine.Random.Range(SpawnInscreenDown.transform.position.y, SpawnInscreenUp.transform.position.y);
        float destinationX = UnityEngine.Random.Range(SpawnInscreenDown.transform.position.x, SpawnInscreenUp.transform.position.x);
        Vector2 destination = new Vector2(destinationX, destinationY);

        return destination;
    }

    private Vector2 bottomSpawnMethod()
    {
        float side = UnityEngine.Random.Range(SpawnBottomLeft.transform.position.x, SpawnBottomRight.transform.position.x);
        Vector2 _spawnPos = new Vector2(side, SpawnBottomLeft.transform.position.y);

        return _spawnPos;
    }

    private Vector2 leftRightSpawnMethod()
    {
        float height = UnityEngine.Random.Range(SpawnPointDownRight.transform.position.y, spawnPointUpLeft.transform.position.y);
        float side = UnityEngine.Random.Range(0, 2) == 1 ? spawnPointUpLeft.transform.position.x : SpawnPointDownRight.transform.position.x;
        Vector2 _spawnPos = new Vector2(side, height);

        return _spawnPos;
    }

    private void initializeWave()
    {
        curStage.currentWaveIndex = 0;

        for (int i = 0; i < curStage.WaveList.Count; i++)
        {
            for (int j = 0; j < curStage.WaveList[i].WaveStat.Count; j++)
            {
                SpawnStat _spawnStat = curStage.WaveList[i].WaveStat[j];

                _spawnStat.RestStat();
                _spawnStat.RandomSpawnTime();
            }
        }
    }

    private void LoadNewWave(WaveScriptable newWave,int _)
    {
        curStage = newWave;
        initializeWave();
    }
}
