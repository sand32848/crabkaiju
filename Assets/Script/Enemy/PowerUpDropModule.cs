using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDropModule : MonoBehaviour
{
    private Health health => GetComponent<Health>();

    [SerializeField] private List<GameObject> powerUpList = new List<GameObject>();
    [SerializeField] private float dropChance;
    [SerializeField] private bool dropOnDestroy = false;

    private void OnEnable()
    {
        health.OnHealthOut += DropPowerup;
    }

    private void OnDisable()
    {
        health.OnHealthOut += DropPowerup;
    }

    public void DropPowerup()
    {
        if (powerUpList.Count == 0) return;

        int random = Random.Range(0,100);

        if(dropChance >= random)
        {
            Instantiate(powerUpList[Random.Range(0,powerUpList.Count)],transform.position,Quaternion.identity);
        }
    }

    private void OnDestroy()
    {
       if(dropOnDestroy) DropPowerup();
    }
}
