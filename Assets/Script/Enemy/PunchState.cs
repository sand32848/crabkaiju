using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchState : State
{
    [SerializeField] private float punchInterval;
    [SerializeField] private HandManager handManager;
    [SerializeField] private int punchCount;
    private int fistIndex;
    private float _punchInterval;
    private int _punchCount;

    private void Start()
    {
        _punchInterval = punchInterval;
    }

    public override void RunCurrentState()
    {
        _punchInterval -= Time.deltaTime;

        if(_punchInterval <= 0)
        {
            Fist fist = handManager.fists[fistIndex];

            if (fist.ready)
            {
                fist.ChooseLocation();

                _punchInterval = punchInterval;
                fistIndex += 1;
                _punchCount += 1;

                if(fistIndex >= handManager.fists.Count)
                {
                    fistIndex = 0;
                }
            }
        }

        if(_punchCount >= punchCount)
        {
            stateMachine.SwitchState(typeof(IdleState));
        }
    }

    public override void OnEnterState()
    {
        handManager.KillAllHandShake();
    }

    public override void OnExitState()
    {
        _punchInterval = 0;
        _punchCount = 0;
        handManager.resetHandPosition();
    }
}
