using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public class MoveModule : MonoBehaviour
{
    private int moveDirX;
    private int moveDirY;
    [SerializeField] private float moveSpeed;
    private bool isAllowMove = true;
    private GrabModule grabModule => GetComponent<GrabModule>();
    private SpawnModule spawnModule => GetComponent<SpawnModule>();
    [SerializeField] private MoveMode moveMode  = MoveMode.LeftRight;
    public Action OnMoveComplete;
    private Vector2 destination;

    private GameObject player => GameObject.FindGameObjectWithTag("Player");

    private void OnEnable()
    {
        if(grabModule) grabModule.OnGrab += stopMoving;
        spawnModule.OnSpawn += SetMoveDir;
    }

    private void OnDisable()
    {
        if (grabModule) grabModule.OnGrab -= stopMoving;
        spawnModule.OnSpawn -= SetMoveDir;
    }

    private void Update()
    {
        if (!isAllowMove) return;

        if (moveMode == MoveMode.LeftRight)
        {
            transform.position += moveSpeed * new Vector3(moveDirX, moveDirY) * Time.deltaTime;
        }
        else if (moveMode == MoveMode.WalkToPlayer)
        {
            Vector2 moveDir = ((Vector2)player.transform.position - (Vector2)transform.position).normalized;
            transform.position += (Vector3)moveDir * moveSpeed * Time.deltaTime;
        }
    }

    public void SetMoveDir(Vector2 _spawnPos, Vector2 _destination)
    {
        if (moveMode == MoveMode.LeftRight)
        {
            int _moveDir = _spawnPos.x < 0 ? 1 : -1;

            moveDirX = _moveDir;
        }
        else if (moveMode == MoveMode.WalkToward)
        {
            setDestination((Vector2)_destination);

            transform.DOMove(destination, moveSpeed).SetSpeedBased().OnComplete(() => { OnMoveComplete?.Invoke(); });
        }
        else if (moveMode == MoveMode.DropDown)
        {
            setDestination((Vector2)_destination);

            transform.DOMove(destination, moveSpeed).SetSpeedBased().OnComplete(() => { OnMoveComplete?.Invoke(); });
        }
    }

    public void stopMoving()
    {
        isAllowMove = false;
    }

    public void setDestination(Vector2 _destination)
    {
        destination = _destination;
    }
}

public enum MoveMode { LeftRight,FixPos,WalkToward,WalkToPlayer,DropDown}