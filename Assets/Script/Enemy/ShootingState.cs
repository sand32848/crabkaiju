using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ShootingState : State
{
    [SerializeField] private float speed;
    [SerializeField] private int amount;
    [SerializeField] private float fireRate;
    [SerializeField] private float burstCooldown;
    [SerializeField] private int burstBeforeSuper;
    [SerializeField] private float laserDelay;

    [SerializeField] private Bullet bullet;
    [SerializeField] private GameObject laser;
    [SerializeField] private GameObject superBulletCannon;
    [SerializeField] private Transform superBulletPoint;
    [SerializeField] private List<GameObject> shootPoints;
    [SerializeField] private List<GameObject> cannonPivot;

    [SerializeField] private int superTotal;
    private int _superTotal;
  
    private bool isShooting = false;

    private float _fireRate;
    private float _burstCooldown;
    private float _burstBeforeSuper;
    private int gunIndex;
    private bool firingLaser;
    private GameObject player => GameObject.FindGameObjectWithTag("Player");
    private bool resettingPosition;

    private void Start()
    {
        _fireRate = fireRate;
        _burstCooldown = burstCooldown;
    }

    public override void RunCurrentState()
    {

        for(int i = 0; i < cannonPivot.Count; i++)
        {
            rotateToPlayer(cannonPivot[i].transform);
        }

        if (_superTotal >= superTotal)
        {
            return;
        }

        if (resettingPosition) return;

        if (gunIndex == 1)
        {
            if (firingLaser) return;
            ShootingSuperBullet();
        }

        _burstCooldown -= Time.deltaTime;

        if (_burstCooldown > 0) return;
        if (isShooting) return;
        if (gunIndex == 1) return;

        StartCoroutine(Shooting()); 
    }

    IEnumerator Shooting()
    {
        isShooting = true;

        for (int i = 0; i < amount; i++)
        {
            for (int j = 0; j < shootPoints.Count; j++)
            {
                Vector2 dirToPlayer = (player.transform.position - shootPoints[j].transform.position).normalized;

                Bullet _bullet = Instantiate(bullet, shootPoints[j].transform.position, Quaternion.identity);
                _bullet.OnSpawn(dirToPlayer);
            }

            yield return new WaitForSeconds(_fireRate);
        }

        _fireRate = fireRate;
        _burstCooldown = burstCooldown;
        _burstBeforeSuper += 1;

        isShooting = false;

        if(_burstBeforeSuper >= burstBeforeSuper)
        {
            gunIndex = 1;
        }
    }

    public void ShootingSuperBullet()
    {
        firingLaser = true;

        DOTween.Kill("MoveLaser");

       // float destination =  UnityEngine.Random.Range(0, 2) == 1 ? -3.4f : 3.4f;
        float destination = player.transform.position.x <=0 ? -3.4f : 3.14f;
        
        float destination2 = destination == 3.4f ? -3.4f : 3.4f;

        transform.DOMoveX(destination, speed).OnComplete(() =>
        {
            laser.transform.DOScaleX(0f, laserDelay).OnComplete(() =>
            {
                laser.transform.DOScaleX(3, 1f).OnComplete(() =>
                {
                    laser.GetComponent<Collider2D>().enabled = true;

                    transform.DOMoveX(destination2, speed).OnComplete(() =>
                    {

                        laser.transform.DOScaleX(0, 0.5f);

                        laser.GetComponent<Collider2D>().enabled = false;

                        gunIndex = 0;
                        firingLaser = false;
                        _burstBeforeSuper = 0;
                        _superTotal += 1;

                        if (_superTotal >= superTotal)
                        {
                            DOTween.Kill("MoveLaser");

                            if (!resettingPosition)
                            {
                                resettingPosition = true;

                                transform.DOMoveX(0, 0.5f).OnComplete(() => { stateMachine.SwitchState(typeof(IdleState)); });
                            }
                        }
                        else
                        {
                            Move();
                        }
                    });
                });
            });
        });
    }

    public override void OnEnterState()
    {
        Move();
    }

    public override void OnExitState()
    {
        DOTween.Kill("MoveLaser");

        for (int i = 0; i < cannonPivot.Count; i++)
        {
            cannonPivot[i].transform.DORotate(new Vector3(0,0,90f), 0.5f);
        }

        superBulletCannon.transform.DORotate(new Vector3(0, 0, 90f), 0.5f);

        _superTotal = 0;
        _burstBeforeSuper = 0;
        gunIndex = 0;
        firingLaser = false;
        isShooting = false;
        resettingPosition = false;
    }

    public  void rotateToPlayer(Transform toRotate) 
    {
        Vector2 dir = (Vector2)player.transform.position - (Vector2)toRotate.position;

        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        toRotate.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void Move()
    {
        float destination = UnityEngine.Random.Range(0, 2) == 1 ? -3.14f : 3.14f;
        float destination2 = destination == 3.14f ? -3.14f : 3.14f;

        var sequence = DOTween.Sequence().SetLoops(-1, LoopType.Yoyo).SetId("MoveLaser");

        sequence.Append(transform.DOMoveX(destination, 2f).SetSpeedBased().SetEase(Ease.Linear));
        sequence.Append(transform.DOMoveX(destination2, 2f).SetSpeedBased().SetEase(Ease.Linear));
    }
}
