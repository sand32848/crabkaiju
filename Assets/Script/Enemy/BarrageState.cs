using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UIElements;

public class BarrageState : State
{
    [SerializeField] private float mortarChargeTime;
    [SerializeField] private Bullet mortarBullet;
    [SerializeField] private GameObject mortarPoint;
    [SerializeField] private GameObject cannon;
    private GameObject player => GameObject.FindGameObjectWithTag("Player");

    private GameObject bullet;

    private bool isFiringMortar;


    public override void RunCurrentState()
    {
        rotateToPlayer(cannon.transform);

        if (bullet) bullet.transform.position = mortarPoint.transform.position;
        if (isFiringMortar) return;
        FireMortar();

    }

    public void FireMortar()
    {
        isFiringMortar = true;

        Bullet _bullet = Instantiate(mortarBullet, mortarPoint.transform.position,Quaternion.identity);

        bullet = _bullet.gameObject;

        _bullet.transform.localScale = Vector3.zero;

        _bullet.transform.DOScale(Vector3.one, mortarChargeTime).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            _bullet.OnSpawn(player.transform.position);
            isFiringMortar = false;
        });


    }

    public void rotateToPlayer(Transform toRotate)
    {
        Vector2 dir = (Vector2)player.transform.position - (Vector2)toRotate.position;

        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        toRotate.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

}
