using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fist : Bullet
{
    [field :  SerializeField] public GameObject originalPos { get; private set; }
    [SerializeField]private float MaxXposition = 8.3f;
    [SerializeField]private float MinXposition = 8.3f;
    private bool isFiring;
    public bool ready { get; private set; } = true;
    private GrabModule grabModule => GetComponent<GrabModule>();


    private void OnEnable()
    {
        grabModule.OnClawHit += ReturnClaw;
    }

    private void OnDisable()
    {
        grabModule.OnClawHit -= ReturnClaw;
    }

    public void ChooseLocation()
    {
        float randomX = UnityEngine.Random.Range(MinXposition, MaxXposition);
        Vector2 destination = new Vector2(randomX, 4);

        ready = false;
        transform.DOMove(new Vector2(destination.x, -3.76f), 0.3f).SetEase(Ease.Linear).SetId("Punch").OnComplete(() =>
        {
            isFiring = true;
            transform.DOMoveY(destination.y, 1f).SetEase(Ease.InExpo).SetId("Punch").OnComplete(() =>
            {
                CameraShakeController.Instance.ShakeCamera(2,0.5f);
                ReturnClaw();
                isFiring = false;
            });
        });
    }

    public override void OnHitTrigger(Collider2D _collision)
    {
        if (_collision.transform.TryGetComponent(out Health health))
        {
            health.ReduceHealth(damage);
        }
    }

    public void ReturnClaw()
    {
        if (!isFiring) return;

        transform.DOMove(originalPos.transform.position, 0.5f);
        ready = true;
    }

    public void setReady()
    {
        ready = true;
    }

 
}
