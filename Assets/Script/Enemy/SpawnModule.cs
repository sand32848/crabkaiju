using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnModule : MonoBehaviour
{
    [field:SerializeField] public SpawnMethod spawnMethod { get;private set; }

    public Action<Vector2,Vector2> OnSpawn;
}

public enum SpawnMethod {LeftRight,Bottom,InScreen,Random,Top,Manual}
