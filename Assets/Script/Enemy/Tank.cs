using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    [SerializeField] private GameObject tankHeaed;
    private GameObject player => GameObject.FindGameObjectWithTag("Player");

    // Update is called once per frame
    void Update()
    {

        Vector2 dirToPlayer = (Vector2)player.transform.position - (Vector2)tankHeaed.transform.position;

        var angle = Mathf.Atan2(dirToPlayer.y, dirToPlayer.x) * Mathf.Rad2Deg;
        tankHeaed.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
