using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LaserState : State
{
    [SerializeField] private float Speed;
    [SerializeField] private int Damage;
    [SerializeField] private GameObject laserBeam;

    public override void RunCurrentState()
    {

    }

    public override void OnEnterState()
    {
       float target = UnityEngine.Random.Range(0, 2) == 1? -3.14f : 3.14f;

        transform.DOMoveX(target, Speed).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() => 
        {
            laserBeam.GetComponent<Collider2D>().enabled = true;
            laserBeam.transform.DOScaleX(3, 1f).OnComplete(() =>
            {
                float target2 = target == 3.14f? -3.14f : 3.14f; ;

                transform.DOMoveX(target2, Speed).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() =>
                {
                    laserBeam.transform.DOScaleX(0f, 1f).OnComplete(() =>
                    {
                        transform.DOMoveX(0, Speed).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() =>
                        {
                            laserBeam.GetComponent<Collider2D>().enabled = false;
                            stateMachine.SwitchState(typeof(IdleState));
                        });
                       
                    });
                });

            });
        });
    }
}
