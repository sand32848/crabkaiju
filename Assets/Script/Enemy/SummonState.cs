using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SummonState : MonoBehaviour
{
    [SerializeField] private int Amount;
    [SerializeField] private float spawnRate;
    [SerializeField] private GameObject miniom;
    private float Xpos = 7.66f;
    private float initialMinYpos = -3.3f;
    private float minYpos = -1.19f;
    private float maxYpos = 0.39f;

    private float _spawnRate;
    
    void Start()
    {
        _spawnRate= spawnRate;
    }

    // Update is called once per frame
    void Update()
    {
        _spawnRate -= Time.deltaTime;

        if (_spawnRate <= 0)
        {
            for(int i =0; i< Amount; i++)
            {
                float initialSide = UnityEngine.Random.Range(0, 2) == 1 ? 10f : -10f;
                float initialY = UnityEngine.Random.Range(initialMinYpos, maxYpos);

                float randomX = UnityEngine.Random.Range(-Xpos, Xpos);
                float randomY = UnityEngine.Random.Range(minYpos, maxYpos);

                print(randomX);
                print(randomY);

                GameObject minion = Instantiate(miniom, new Vector2(initialSide, initialY), Quaternion.identity);

                minion.transform.DOMove(new Vector2(randomX, randomY), 2f);
            }
            _spawnRate = spawnRate;
        }
    }


}
