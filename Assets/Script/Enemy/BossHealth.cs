using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour
{
    public static Action<int,int> OnUpdateBossHealth;
    public static Action OnBossDeath;
    [SerializeField] private StateMachine stateMachine;
    [SerializeField] private State phase2State;
    [SerializeField] private ParticleSystem woundParticle;
    [SerializeField] private GameObject bodyPart;
    [SerializeField] private GameObject bodyPart2;

    private Health health;

    private bool phase2;

    private void Start()
    {
        health= GetComponent<Health>();
        health.OnHealthChange += UpdateBossHealth;
    }

    private void OnDisable()
    {
        health.OnHealthChange -= UpdateBossHealth;
    }

    public void UpdateBossHealth(int curHealth,int maxHealth)
    {
        if ((float)curHealth / (float)maxHealth <= 0.3)
        {
            bodyPart.transform.DOShakePosition(9999f,0.2f,1,90);
            bodyPart2.transform.DOShakePosition(9999f, 0.2f, 1, 90);

            stateMachine.SwitchState(phase2State);
            woundParticle?.Play();
        }

        if((float)curHealth / (float)maxHealth <= 0.0)
        {
            OnBossDeath?.Invoke();
        }

        OnUpdateBossHealth?.Invoke(curHealth,maxHealth);
    }
}
