using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [field: SerializeField] public List<State> stateList { get; private set; } = new List<State>();
    [SerializeField] private State currentState;

    private void Start()
    {
        for(int i = 0; i< stateList.Count; i++)
        {
            stateList[i].setStateMachine(this);
        }

        currentState.OnEnterState();
    }

    private void Update()
    {
        currentState?.RunCurrentState();
    }

    public void SwitchState(System.Type _state) 
    {
        currentState.OnExitState();
        State state = stateList.Where(x => x.GetType() == _state).First();

        currentState = state;
        currentState.OnEnterState();
    }

    public void SwitchState(State _state)
    {
        currentState.OnExitState();
        currentState = _state;
        currentState.OnEnterState();
    }
}
