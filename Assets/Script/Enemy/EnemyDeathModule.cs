using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathModule: MonoBehaviour
{
    public static Action OnEnemyDeath;
    public Action OnDeathWall;
    public GrabModule grabModule => GetComponent<GrabModule>();
    public Health health => GetComponent<Health>();

    private void OnEnable()
    {
        if(grabModule) grabModule.OnEat += Death;
        health.OnHealthOut += Death;
    }

    private void OnDisable()
    {
        if (grabModule) grabModule.OnEat -= Death;
        health.OnHealthOut -= Death;
    }

    public void Death()
    {
        OnEnemyDeath?.Invoke();
    }
}
