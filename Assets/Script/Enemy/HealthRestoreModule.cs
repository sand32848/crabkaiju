using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRestoreModule : MonoBehaviour
{
    private GrabModule grabModule => GetComponent<GrabModule>();
    private Health health;
    [SerializeField] private int healthRestore;

    private void Start()
    {
        health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
    }

    private void OnEnable()
    {
        grabModule.OnEat += RestoreHealth;
    }

    private void OnDisable()
    {
        grabModule.OnEat -= RestoreHealth;
    }

    public void RestoreHealth()
    {
        health.IncreaseHealth(healthRestore);
    }
}
