using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGunModule : MonoBehaviour
{
    private GrabModule grabModule => GetComponent<GrabModule>();
    [SerializeField] private int gunIndex;
    private GunController gunController;

    private void Start()
    {
        gunController = GameObject.FindGameObjectWithTag("Player").GetComponent<GunController>();
    }

    private void OnEnable()
    {
        grabModule.OnEat += ChangeGun;
    }

    private void OnDisable()
    {
        grabModule.OnEat += ChangeGun;
    }

    public void ChangeGun()
    {
        gunController.SwitchGun(gunIndex);
    }
}
