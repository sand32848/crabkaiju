using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackModule : MonoBehaviour
{
    [SerializeField] private bool allowAttack;
    [SerializeField] private float attackCooldown;
    [SerializeField] private float spread;
    [SerializeField] private int amount  = 1;

    [SerializeField] private Bullet bullet;
    [SerializeField] private GameObject attackParticle;

    private GameObject player;
    private float _attackCooldown;

    private MoveModule moveModule => GetComponent<MoveModule>();

    private void OnEnable()
    {
       moveModule.OnMoveComplete += AllowAttack;
    }

    private void OnDisable()
    {
        moveModule.OnMoveComplete -= AllowAttack;
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _attackCooldown = attackCooldown;
    }

    void Update()
    {
        if (!player) return;

        if (!allowAttack) return;

        _attackCooldown -= Time.deltaTime;

        if(_attackCooldown <= 0)
        {
           if(attackParticle)  Destroy(Instantiate(attackParticle, transform.position, Quaternion.identity), 0.3f);

            Vector2 bulletDir = dirToPlayer();
            bulletDir = Quaternion.AngleAxis(-spread, Vector3.forward) * bulletDir;

            float step = spread / (float) amount;

            for (int i =0; i  < amount; i++)
            {
                bulletDir = Quaternion.AngleAxis(step, Vector3.forward) * bulletDir;

                Bullet _bullet = Instantiate(bullet, transform.position, bullet.transform.rotation);
                _bullet.getPlayer(player);
                _bullet.OnSpawn(bulletDir);

                _attackCooldown = attackCooldown;
            }
        }
    }

    public void AllowAttack()
    {
        allowAttack = true;
    }

    private Vector2 dirToPlayer()
    {
        Vector2 dir = (player.transform.position - transform.position).normalized;

        return dir;
    }
}
