using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Runtime.CompilerServices;
using System;

public class BossDeath : MonoBehaviour
{
    [SerializeField] private float explodeTime;
    [SerializeField] private HandManager handManager;
    [SerializeField] private ParticleSystem explodeParticle;
    [SerializeField] private ParticleSystem burstParticle;
    [SerializeField] private List<GameObject> disableList = new List<GameObject>();
    [SerializeField] private List<Rigidbody2D> partList = new List<Rigidbody2D>();
    [SerializeField] private GameObject laser;
    public static Action onCallCutscene;

    [SerializeField] private StateMachine stateMachine ;
    [SerializeField] SummonState summonState;
    private Health health => GetComponent<Health>();
    private bool isDead;
    

    private void OnEnable()
    {
        health.OnHealthOut += BossDeathPlay;
    }

    private void OnDisable()
    {
        health.OnHealthOut -= BossDeathPlay;
    }

    public void BossDeathPlay()
    {
        if (isDead) return;
        isDead = true;

        stateMachine.enabled = false;
        summonState.enabled = false;
        laser.SetActive(false);
        
        explodeParticle.Play();

        handManager.resetHandPosition();

        for (int i = 0; i < partList.Count; i++)
        {
            partList[i].transform.DOShakePosition(999, 0.2f, 1, 90).SetId("Shake");
        }

        transform.DOMoveX(0,1f).SetEase(Ease.Linear).OnComplete(()=> 
        {
            DOTween.To(() => explodeTime, x => explodeTime = x, 0, 2).OnComplete(() => { ExplodePart(); });
        });
    }

    public void ExplodePart()
    {
        DOTween.Kill("Shake");
        explodeParticle.Stop();
        burstParticle.Play();

        for (int i = 0; i < disableList.Count; i++)
        {
            disableList[i].SetActive(false);
        }

        for (int i = 0; i < partList.Count; i++)
        {
            float randomY = UnityEngine.Random.Range(15, 20);
            float randomX = UnityEngine.Random.Range(-10, 10);

            partList[i].AddForce(new Vector2(randomX,randomY),ForceMode2D.Impulse);
        }

        transform.DOMoveX(0, 2f).OnComplete(() =>
        {
            onCallCutscene.Invoke();
        });
    }
}
