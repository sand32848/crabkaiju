using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabModule : MonoBehaviour
{
    [field: SerializeField] public bool Grabable { get; private set; } = true;
    public Action OnGrab;
    public Action OnEat;
    public Action OnClawHit;
}

