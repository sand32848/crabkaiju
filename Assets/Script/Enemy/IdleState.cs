using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    private State PreviousState;

    public override void RunCurrentState()
    {

    }

    public override void OnEnterState()
    {
        List<State> states = new List<State>(stateMachine.stateList);

        states.Remove(PreviousState);
        states.Remove(this);

        State goToState = states[Random.Range(0, states.Count)];
        PreviousState = goToState;

        stateMachine.SwitchState(goToState.GetType());
    }
}
