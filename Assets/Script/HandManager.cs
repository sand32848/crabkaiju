using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HandManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> originalPos = new List<GameObject>();
    [SerializeField] private List<LineRenderer> lineRenderers = new List<LineRenderer>();
    [field:SerializeField] public List<Fist> fists= new List<Fist>();

    private void Start()
    {
        ShakeHand();
    }

    void Update()
    {
        SetArmLine();
    }

    public  void SetArmLine()
    {

        for(int i =0; i< lineRenderers.Count; i++)
        {
            lineRenderers[i].SetPosition(0,lineRenderers[i].transform.position);
            lineRenderers[i].SetPosition(1, fists[i].transform.position);
        }
    }

    public void ShakeHand()
    {
        for (int i = 0; i < lineRenderers.Count; i++)
        {
            fists[i].transform.DOShakePosition(999999, 0.5f, 1, 90, false, false).SetId("Shake");
        }
    }

    public void KillAllHandShake()
    {
        DOTween.Kill("Shake");
    }

    public void resetHandPosition()
    {
        for (int i = 0; i < fists.Count; i++)
        {
            fists[i].transform.DOMove(fists[i].originalPos.transform.position, 0.5f);
            fists[i].setReady();

            DOTween.Kill("Punch");
        }
    }
}
