using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;
using System;

public class GunController : MonoBehaviour
{
    [SerializeField] private Gun currentGun;

    [SerializeField] private List<Gun> gunList  = new List<Gun>();

    [SerializeField] private ParticleSystem gunChangeParticle;

    public static Action<int> OnGunSwitch;

    private Vector2 mousePos;
    private bool isFiring;


    private void Start()
    {
        GlobalInputController.Instance.playerAction.Player.GunFire.started += OnFire;
        GlobalInputController.Instance.playerAction.Player.GunFire.canceled += OnFire;

        gunList.ForEach(gun => gun.gunController = this);
    }


    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.Player.GunFire.started -= OnFire;
        GlobalInputController.Instance.playerAction.Player.GunFire.canceled -= OnFire;
    }

    private void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>());

        Vector2 IdleDir = (mousePos - (Vector2)currentGun.transform.position);

        var angle = Mathf.Atan2(IdleDir.y, IdleDir.x) * Mathf.Rad2Deg;
        currentGun.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        if (isFiring)
        {
            if (currentGun.currentBulletAmount <= 0)
            {
                SwitchGun(0);
            }

            currentGun.HoldFire();
        }
    }

    public void OnFire(InputAction.CallbackContext context)
    {
        isFiring = context.ReadValueAsButton();

        if(currentGun.currentBulletAmount <= 0)
        {
            SwitchGun(0);
        }

        if (isFiring)
        {
            currentGun.SingleFire();
        }
        else
        {
            currentGun.ReleaseFire();
        }

    }

    public void SwitchGun(int gunIndex)
    {
        if (gunIndex == -1) return;

        if (currentGun.tier > gunList[gunIndex].tier && currentGun.currentBulletAmount > 0) return;

        if (gunList[gunIndex] != currentGun)
        {
            gunList.ForEach(gun => gun.gameObject.SetActive(false));
            currentGun = gunList[gunIndex];
            currentGun.transform.localScale = Vector3.zero;
            currentGun.transform.DOScale(Vector3.one, 0.5f);
            currentGun.gameObject.SetActive(true);

            gunChangeParticle.Play();
        }

        OnGunSwitch?.Invoke(gunIndex);

        currentGun.OnSwitchGun();
        currentGun.RestartBullet();
    }
}
