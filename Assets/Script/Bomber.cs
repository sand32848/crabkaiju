using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;

public class Bomber : MonoBehaviour
{
    [SerializeField] private float triggerDistance;
    [SerializeField] private float explodeRadius;
    [SerializeField] private int damage;
    [SerializeField] private LayerMask hitMask;
    [SerializeField] private GameObject explodeParticle;
    private bool allowExplode = true;
    private Health health => GetComponent<Health>();
    private GrabModule grabModule => GetComponent<GrabModule>();

    private void OnEnable()
    {
        health.OnHealthOut += Explode;
        grabModule.OnGrab += DisableExplosive;
    }

    private void OnDisable()
    {
        health.OnHealthOut -= Explode;
        grabModule.OnGrab += DisableExplosive;
    }

    private void Update()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, triggerDistance, LayerMask.GetMask("Player"));

        if(collider.Length > 0)
        {
            Explode();
        }
    }

    public void Explode()
    {
        if (!allowExplode) return;

        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position,explodeRadius,hitMask);

        for (int i = 0; i < collider.Length; i++)
        {
            if (collider[i].transform.gameObject == this.gameObject) continue;

            if (collider[i].TryGetComponent(out Health health))
            {
                health.ReduceHealth(damage);
            }
        }

        if(explodeParticle) Destroy(Instantiate(explodeParticle, transform.position, Quaternion.identity),1);
        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, explodeRadius);
        Gizmos.DrawWireSphere(transform.position, triggerDistance);
    }

    public void DisableExplosive()
    {
        allowExplode = false;
    }
}
