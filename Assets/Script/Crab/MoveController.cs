using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float moveSpeed;
    void Start()
    {
       rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void Move()
    {
        rb.velocity = new Vector2(GlobalInputController.Instance.playerAction.Player.Movement.ReadValue<Vector2>().x,0) * moveSpeed;
    }
}
