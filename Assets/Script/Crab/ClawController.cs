using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;


public class ClawController : MonoBehaviour
{
    [SerializeField] private Claw claw;
    [SerializeField] private GameObject clawPoint;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private ParticleSystem chargeParticle;

    [SerializeField] private float clawSpeed;
    [SerializeField] private float clawBackSpeed;

    [SerializeField] private float chargeTime;

    [SerializeField] private AudioSource clawChargeSound;

    private Vector2 clawDir;
    private Vector3 clawBackDir;
    private Vector2 mousePos;
    public bool isFiring { get; private set; }
    public bool isReturning { get; private set; }
    private bool isCharging;


    private float currentCharge;


    private void Start()
    {
        GlobalInputController.Instance.playerAction.Player.Fire.started += StartCharge;
        GlobalInputController.Instance.playerAction.Player.Fire.canceled += CancleCharge;
    }


    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.Player.Fire.started -= StartCharge;
        GlobalInputController.Instance.playerAction.Player.Fire.canceled -= CancleCharge;
    }

    private void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>());

        lineRenderer.SetPositions(new Vector3[] { clawPoint.transform.position, claw.transform.position });

        Charge();

        if (isFiring)
        {
            if(currentCharge >= chargeTime)
            {
                claw.transform.position += (Vector3)clawDir * clawSpeed * Time.deltaTime * 5;
                claw.SetCharge(true);
            }
            else
            {
                claw.transform.position += (Vector3)clawDir * clawSpeed * Time.deltaTime;
            }
        }
        else if (isReturning)
        {
            clawBackDir = clawPoint.transform.position - claw.transform.position;

            claw.transform.position += (clawBackDir.normalized * clawBackSpeed * Time.deltaTime);

            Vector2 BackDir = (Vector2)claw.transform.position - (Vector2)clawPoint.transform.position;

            var angle = Mathf.Atan2(BackDir.y, BackDir.x) * Mathf.Rad2Deg;
            claw.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            //Retract Claw
            if (Vector2.Distance(claw.transform.position, clawPoint.transform.position) <= 0.5f)
            {
                isReturning = false;

                claw.transform.parent = gameObject.transform;

                claw.SetCharge(false);
                isCharging = false;
                currentCharge = 0;

                if (!claw.inClawObject) return;

                claw.inClawObject.GetComponent<GrabModule>().OnEat?.Invoke();
                Destroy(claw.inClawObject);
            }
        }
        else  //Idle
        {
            claw.transform.position = clawPoint.transform.position;

            lineRenderer.enabled = false;

            Vector2 IdleDir = (mousePos - (Vector2)claw.transform.position);

            var angle = Mathf.Atan2(IdleDir.y, IdleDir.x) * Mathf.Rad2Deg ;
            claw.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward) ;
        }
    }

    public void StartCharge(InputAction.CallbackContext context)
    {
        if (isFiring) return;

        if (isReturning) return;

        isCharging = true;

        chargeParticle.Play();
    }

    public void CancleCharge(InputAction.CallbackContext context)
    {
        if (isFiring) return;

        if (isReturning) return;

        clawDir = (mousePos - (Vector2)claw.transform.position).normalized;

        isFiring = true;

        lineRenderer.enabled = true;

        claw.transform.parent = null;

        chargeParticle.Stop();

        if (currentCharge >= chargeTime)
        {
            CameraShakeController.Instance.ShakeCamera(3f,0.3f);
            clawChargeSound.Play();
        }

    }

    public void Charge()
    {
        if (isCharging)
        {
            currentCharge += Time.deltaTime;

            if (currentCharge >= chargeTime)
            {
                var main = chargeParticle.main;
                main.startColor = Color.red;
            }
            else
            {
                var main = chargeParticle.main;
                main.startColor = Color.white;
            }
        }

 
    }

    public void SetReturn()
    {
        isFiring = false;
        isReturning = true;
    }
}
