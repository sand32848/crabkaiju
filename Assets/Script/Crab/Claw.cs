using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Claw : MonoBehaviour
{
    [SerializeField] private GameObject holder;
    [SerializeField] private ClawController clawController;
    [SerializeField] private int clawChargeDamage;
    [SerializeField] private int clawDamage;
    public GameObject inClawObject { get; private set; }
    private bool isCharge;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!clawController.isFiring  && !clawController.isReturning) return;

        if (isCharge)
        {
            if (!inClawObject)
            {
                if (collision.transform.TryGetComponent(out GrabModule _grab))
                {
                    if (_grab.Grabable)
                    {
                        setGrabObject(collision.transform);

                        _grab.OnGrab?.Invoke();

                        print("D");
                    }
                }
            }

            if (inClawObject != collision.transform.gameObject)
            {
                if (collision.transform.TryGetComponent(out Health health))
                {
                    health.ReduceHealth(clawChargeDamage);
                }

            }

            if (collision.transform.tag == "Wall") clawController.SetReturn();

            return;
        }

        if (inClawObject) return;

        if (collision.transform.TryGetComponent(out GrabModule grab))
        {
            if (grab.Grabable)
            {
                setGrabObject(collision.transform);

                grab.OnGrab?.Invoke();
            }
            else
            {
                grab.OnClawHit?.Invoke();
                grab.transform.GetComponent<Health>()?.ReduceHealth(clawDamage);
            }
        }

        clawController.SetReturn();

    }
    public void SetCharge(bool chargeState)
    {
        isCharge = chargeState;
    }

    public void setGrabObject(Transform grabTransform)
    {
        inClawObject = grabTransform.gameObject;
        grabTransform.GetComponent<Collider2D>().enabled = false;
        grabTransform.position = holder.transform.position;
        grabTransform.parent = transform;
        
    }
}
