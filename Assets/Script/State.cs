using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State : MonoBehaviour
{
    [SerializeField]protected StateMachine stateMachine;

    public abstract void RunCurrentState();

    public virtual void OnExitState() { }

    public virtual void OnEnterState() { }

    public void setStateMachine(StateMachine _stateMachine)
    {
        stateMachine = _stateMachine;
    }
}
