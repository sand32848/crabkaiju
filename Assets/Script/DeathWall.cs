using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWall : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.TryGetComponent(out EnemyDeathModule enemyDeathModule))
        {
            enemyDeathModule.OnDeathWall?.Invoke();
        }

        Destroy(collision.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.transform.TryGetComponent(out EnemyDeathModule enemyDeathModule))
        {
            enemyDeathModule.OnDeathWall?.Invoke();
        }

        Destroy(collision.gameObject);

    }
}
