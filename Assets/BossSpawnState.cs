using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawnState : State
{
    public static Action OnBossSpawn;
    public static Action OnBossHealthBar;
    private float destination = -4f;
    private HandManager handManager => GetComponent<HandManager>();
    [SerializeField] private Collider2D hitBox;

    private void Start()
    {
        hitBox.enabled = false;
    }

    public override void RunCurrentState()
    {
        
    }

    private void OnEnable()
    {
        GameManager.OnBossWaveReach += InitiateBoss;
    }

    private void OnDisable()
    {
        GameManager.OnBossWaveReach -= InitiateBoss;
    }

    public void InitiateBoss()
    {
        OnBossSpawn?.Invoke();
        MusicPlayer.Instance.SwitchMusic(-1);

        transform.DOMoveY(transform.position.y, 2f).OnComplete(() => 
        {
            transform.DOMoveY(destination, 2f).OnComplete(() =>
            {
                OnBossHealthBar?.Invoke();
                MusicPlayer.Instance.SwitchMusic(2);

                transform.DOMove(transform.position, 2f).OnComplete(() =>
                {
                    
                    stateMachine.SwitchState(typeof(IdleState));
                    hitBox.enabled = true;
                    GetComponent<SummonState>().enabled = true;
                });
              
            });
            ; });

       
    }
}
