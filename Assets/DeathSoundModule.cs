using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSoundModule : MonoBehaviour
{
   Health health =>  GetComponent<Health>();
    [SerializeField] private List<AudioSource> audioSource = new List<AudioSource>();


    private void OnEnable()
    {
        health.OnHealthOut += PlayDeathSound;
    }

    private void OnDisable()
    {
        health.OnHealthOut -= PlayDeathSound;

    }

    public void PlayDeathSound()
    {

    }
}
