using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabAnimator : MonoBehaviour
{
    [SerializeField] private GameObject body;
    private Animator animator => GetComponent<Animator>();

    private void Start()
    {
        BodyAnimator();
    }

    void Update()
    {
        if(GlobalInputController.Instance.playerAction.Player.Movement.ReadValue<Vector2>() != Vector2.zero)
        {
            animator.Play("Move");
        }
        else
        {
            animator.Play("Idle");
        }
    }

    public  void BodyAnimator()
    {
        var bodySequence = DOTween.Sequence().SetLoops(-1,LoopType.Yoyo);

        bodySequence.Append(body.transform.DOLocalMoveY(0f,1f));
        bodySequence.Append(body.transform.DOLocalMoveY(0.1f, 1f));

    }

    public void MouthAnimator()
    {

    }

    public  void EyeAnimator()
    {

    }
}
